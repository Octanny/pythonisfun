#!/usr/bin/python

#compare email addresses using the first letter
def compare(email1, email2):
    if email1[0] < email2[0]:
        return -1
    elif email1[0] > email2[0]:
        return 1
    else:
        return 0

def sortEmailAddressesFromFile( filename ): 
	#read emails from input file
	with open(filename) as fin:
    		content = fin.readlines()
			
	#remove \n from the end of the emails
	content = [line.rstrip('\n') for line in content]
	
	#sort alphabetically after the first letter
	content.sort(compare)
	
	#display sorted emails
	for adr in content:
		print adr
	return;

sortEmailAddressesFromFile("input.txt")