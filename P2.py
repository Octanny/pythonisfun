#!/usr/bin/python

#Takes 2 clock times as input
print "Input format: hh:mm"
firstClock=raw_input("First clock: ");
secondClock=raw_input("Second clock: ");

#convert the clock times to an integer (e.g.: 12:36 --> 1236)
hm1=int(firstClock.replace(":", ""))
hm2=int(secondClock.replace(":", ""))

#establish order relationship
if (hm1 > hm2):
	print firstClock + " > " + secondClock
elif hm1 == hm2:
	print firstClock + " = " + secondClock
else:
	print firstClock + " < " + secondClock

